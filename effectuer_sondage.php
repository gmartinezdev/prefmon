<?php
/* 
  Copyright 2016 SamuelBF
  Copyright 2016 Alexis Bienvenüe

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/* effectuer_sondage.php : tâche appelée par CRON qui exécute les sondages prévus 
   dans le fichier sondages.ini */


include_once 'configuration.php';
include_once 'log.class.php';
include_once 'sondages.php';
include_once 'periodes.class.php';

# Lecture des options passées en ligne de commande ou dans "$options" :
# -d : messages de débogage
# -n : ne pas enregistrer les résultats en bdd
# -t target : ne traite que le sondage target
# -m nom_min : ne traite que les sondages dont le nom est >= à nom_min
# -M nom_max : ne traite que les sondages dont le nom est <= à nom_min
# -s systeme : ne traite que les sondages du système systeme (EZBooking_41, Eappointment, ...)
#              indiquer '!système' pour inverser la sélection
# -a dossier : dossier où sont stockées les archives
# --proxy p : utilise un proxy pour les requêtes HTTP
if (!isset($options)) {
  $options = getopt("dnt:m:M:s:a:",array("proxy::"));
  $options['logf'] = Log::TERM;
}
if (isset($options['s'])) {
  $options['s_inverser'] = ($options['s'][0] == '!');
  if ($options['s_inverser']) {
    $options['s'] = substr($options['s'], 1);
  }
}
// On s'autorise de modifier la configuration en utilisant options :
$configuration = array_merge($configuration, $options);

error_reporting(E_STRICT);

# Option -d : messages de débogage
$log_level=Log::AVERTISSEMENT;
if(isset($options["d"])) {
  $log_level=Log::DEBUG;
}

$log = new Log($log_level,CHEMIN_ABSOLU.'/'.$configuration['logs'].'/'.date('Y-m-d').'.log', true, $options['logf']);

error_reporting(E_ALL);
# Fonction ouvrir_activite : renvoie un pointeur vers le fichier activite mis à zéro et son
# contenu décodé
function ouvrir_activite($log, $activite_nom) {
  if(!file_exists($activite_nom)) {
    $log->message(Log::DEBUG, 'Création du fichier '.$activite_nom);
    $activite_f = fopen($activite_nom, 'w');
    if(!$activite_f) $log->message(Log::AVERTISSEMENT, 'On a pas réussi à créer le fichier '.$activite_nom);
    return(array($activite_f, 0, array(), array()));
  } else {
    $log->message(Log::DEBUG, 'Ouverture du fichier '.$activite_nom);
    $activite_f = fopen($activite_nom, 'c+');
    if(!$activite_f) { 
      $log->message(Log::AVERTISSEMENT, 'On a pas réussi à ouvrir le fichier '.$activite_nom);
      return(array($activite_f, 0, array(), array()));
    }
    $txt = fread($activite_f, 8192);
    $json = json_decode($txt, true);
    $i = (is_null($json[0])) ? 0:$json[0];
    $en_cours = (is_null($json[1])) ? array():$json[1];
    $finis = (is_null($json[2])) ? array():$json[2];
    ftruncate($activite_f, 0);
    rewind($activite_f);
    return(array($activite_f, $i, $en_cours, $finis));
  }
}

function couleur($datesondage, $date1, $date2) {
  if (empty($date1) || $date1 == 'NULL')
    return "noir";

  $interv1 = (new DateTime($datesondage))->diff(new DateTime($date1));
  if($interv1->y > 0 or $interv1->m > 1)
    return "rouge";

  if($interv1->y == 0 and $interv1->m == 1)
    return "orange";
  
  if(empty($date2) or $date2 == 'NULL' or (new DateTime($datesondage))->diff(new DateTime($date2))->m >= 1)
    return "jaune";

  return "vert";
}

# Parcours de sondages.ini pour effectuer les sondages voulus :
$req_sql = 'INSERT INTO Sondages(IDProcédure, DateSondage, HeureDébut, HeureFin, Date1, Date2) VALUES ';

# Lignes de résultats (sous format SQL '(XXX, XXX, XXX, XXX)')
$lignes = [];

$log->message(Log::DEBUG, 'Lecture du fichier sondages.ini');
if(!$sondages = parse_ini_file(CHEMIN_ABSOLU.'/sondages.ini', true)) {
  $log->message(Log::ERREUR, 'Impossible de lire le fichier '.CHEMIN_ABSOLU.'/sondages.ini. Abandon.');
  exit(255);
}
# Note : si on pense que la vitesse d'exécution du script est un facteur important, on pourrait
# paralléliser l'exécution des différents sondages.
foreach($sondages as $titre=>$paramètres) {
  # Héritage : [91/evry] hérite de [91], puis [91/evry/etudiant] hérite de [91/evry] ...
  if($fin = strrpos($titre, '/')) {
    $log->message(Log::DEBUG, 'La section '.$titre.' hérite des paramètres de la section '.substr($titre, 0, $fin));
    $paramètres = array_merge($sondages[substr($titre, 0, $fin)], $paramètres);
    $sondages[$titre] = $paramètres;
  }

  # Avec l'option -t target, ne traite que le sondage target
  if(isset($options["t"]) && $titre != $options["t"]) {
    $log->message(Log::DEBUG,'Section '.$titre.' ignorée');
  # Avec les options -m -M on limite la liste des sondages à traiter
  } elseif(isset($options["m"]) && $titre < $options["m"]) {
   $log->message(Log::DEBUG,'Section '.$titre.' en-dessous de '.$options["m"]);
  } elseif(isset($options["M"]) && $titre > $options["M"]) {
   $log->message(Log::DEBUG,'Section '.$titre.' au-dessus de '.$options["M"]);
  } elseif(isset($options['s']) &&
    (($options['s_inverser'] && $paramètres['système'] == $options['s']) ||
    (!$options['s_inverser'] && $paramètres['système'] != $options['s']))) {
   $log->message(Log::DEBUG,'Section '.$titre.', ayant pour système '.$paramètres['système'].', est ignorée.');
  # On ne traite pas non plus les sondages inactifs aujourd'hui
  } elseif(isset($paramètres['périodes']) && !(new Periodes($paramètres['périodes']))->est_valide((new DateTime('now'))->format('Y-m-d'))) {
    $log->message(Log::DEBUG,'Section '.$titre.' désactivée (pour le moment)');
  } else {
  
    # On exécute les sondages qu'il y a lieu d'effectuer :
    if(isset($paramètres['IDProcédure'])) {
      list($activite_f, $i, $en_cours, $finis) = ouvrir_activite($log, CHEMIN_ABSOLU.'/'.$configuration['archives'].'/activite.json');
      if($activite_f != NULL) {
        $en_cours[intval($paramètres['IDProcédure'], 16)] = array($paramètres['nom'], date('Y-m-d H:i:s'));
        fwrite($activite_f, json_encode(array($i, $en_cours, $finis)));
        fclose($activite_f);
      }
      $sondage = exécuter_sondage($log, CHEMIN_ABSOLU.'/'.$configuration['archives'].'/'.$titre, array_merge($options,$paramètres,$configuration));
      $lignes = array_merge($lignes, $sondage->versSQL());
      list($activite_f, $i, $en_cours, $finis) = ouvrir_activite($log, CHEMIN_ABSOLU.'/'.$configuration['archives'].'/activite.json');
      if($activite_f != NULL) {
        $date = $en_cours[intval($paramètres['IDProcédure'], 16)][1];
        unset($en_cours[intval($paramètres['IDProcédure'], 16)]);
        $resultat0 = $sondage->résultat[0] ?? 'NULL';
        $resultat1 = $sondage->résultat[1] ?? 'NULL';
        $finis[$i++ % 10] = array(intval($paramètres['IDProcédure'],16), $paramètres['nom'], $date, couleur(date('Y-m-d'), $resultat0, $resultat1), $resultat0, $resultat1);
        if(!fwrite($activite_f, json_encode(array($i, $en_cours, $finis)))) $log->message(Log::AVERTISSEMENT, 'Erreur d\'écriture dans le fichier d\'activité.');
        fclose($activite_f);
      }

      # Si le sondage correspondant à l'option -t a été effectué, ne
      # continue pas avec les autres sondages
      if(isset($options["t"])) break;
    }
  }
}

# Fin de compilation des données, enregistrement dans la base :
if (count($lignes) and !isset($options["n"])) {
  if($configuration['sql']['serveur']) {
    $req_sql .= implode($lignes, ',').';';

    # Enregistrement dans la base :
    $log->message(Log::DEBUG, "Ouverture de la connexion SQL (hôte ".$configuration['sql']['serveur'].")");
    $sql = new mysqli($configuration['sql']['serveur'], $configuration['sql']['utilisateur'],
    $configuration['sql']['motdepasse'], $configuration['sql']['basededonnées']);

    if ($sql->connect_errno) {
      $log->message(Log::ERREUR, "Erreur de connexion SQL ($sql->connect_errno) : $sql->connect_error");
      $log->message(Log::ERREUR, "Requête qui devait être faite : \n" . $req_sql);
      exit;
    }
    $sql->query('SET NAMES UTF8;');
  
    $log->message(Log::DEBUG, 'Exécution de la requête : '.$req_sql);
  
    if(!$sql->query($req_sql)) {
      $log->message(Log::ERREUR, "Erreur d'exécution de la requête SQL ($sql->errno) : $sql->error");
      $log->message(Log::ERREUR, "Requête qui devait être faite : \n" . $req_sql);
    }

    $log->message(Log::DEBUG, "Fermeture de la connexion SQL");
    $sql->close();
  }
}

?>
